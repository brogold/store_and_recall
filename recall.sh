# !/bin/sh
# Title: recall
# Author: 
# Date: 00/00/0000
# Purpose: recall tips and notes from store database file
# Update: 

# consider adding an if statment to print out usage details if the user passes in a "-h"
# consider adding a log file that includes who ran the script and what arguments that passed in.

# variables
store_data="$HOME/scripts/store.db"
if [ "$#" -eq 0 ] ; then
 more $store_data
else
 # note ${PAGER:-more} allows default pager to be used unless nothing set then more used.
 grep -i "$@" $store_data | ${PAGER:-more}
fi



