# README.txt
Three files used in the initial version of this solution.
store.sh, recall.sh and store.db.

store.sh script will store provided commands by writing them to a file
called store.db

recall.sh will print to screen any lines found in store.db based on the
query provided.  If not query is provided the default behaviour is to 
print all the contents of the store.db file.

store.db is not included by default but will be created on the first use
of the store.sh script or it can be defined before running the script.

